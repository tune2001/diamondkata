﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Constraints;

namespace DiamondKata2
{
    public class Diamond
    {
        public List<string> drawDiamond(char sourceLetter)
        {
            if (sourceLetter == 'A')
            {
                return new List<string>(){ "A" };
            }
            
            var TopLinePaddingLength = TopLinePadding(sourceLetter);
            var topLine = new string('-', TopLinePaddingLength) + "A" + new string('-', TopLinePaddingLength);
            
            var midlines = new List<string>();
            for (var currentChar = (Int32)'B'; currentChar <= (Int32)sourceLetter; currentChar++)
            {
                var midLinePaddingLength = (Int32) sourceLetter - currentChar;
                var midLineMiddleLength = MiddleLength((char) currentChar);
                var midLine = new string('-', midLinePaddingLength) + (char)currentChar + new string('-', midLineMiddleLength) + (char)currentChar + new string('-', midLinePaddingLength);

                midlines.Add(midLine);
            }

            var output = new List<string>();

            output.Add(topLine);
            output.AddRange(midlines);

            var bottomHalf = new List<string>();
            bottomHalf.AddRange(output);
            bottomHalf.Reverse();
            bottomHalf.RemoveAt(0);

            output.AddRange(bottomHalf);
            
            return output;
        }

        private int TopLinePadding(char letter)
        {
            return ((Int32) letter - 65);
        }

        private int MiddleLength(char letter)
        {
            return (((Int32)letter - 65)*2)-1;
        }
    }
}