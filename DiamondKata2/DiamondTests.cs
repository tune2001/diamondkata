﻿using System;
using NUnit.Framework;

namespace DiamondKata2
{
    [TestFixture]
    public class DiamondTests
    {
        [Test]
        public void AisA()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('A');

            Assert.That(output[0], Is.EqualTo("A"));
        }

        [Test]
        public void BGivesAOfWidth3()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('B');

            Assert.That(output[0], Is.EqualTo("-A-"));
            Assert.That(output[1], Is.EqualTo("B-B"));
        }

        [Test]
        public void CHasTopHalfDiamond()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('C');

            Assert.That(output[0], Is.EqualTo("--A--"));
            Assert.That(output[1], Is.EqualTo("-B-B-"));
            Assert.That(output[2], Is.EqualTo("C---C"));
        }

        [Test]
        public void DHasTopHalfDiamond()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('D');

            Assert.That(output[0], Is.EqualTo("---A---"));
            Assert.That(output[1], Is.EqualTo("--B-B--"));
            Assert.That(output[2], Is.EqualTo("-C---C-"));
            Assert.That(output[3], Is.EqualTo("D-----D"));
        }

        [Test]
        public void DHasBottomHalfDiamond()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('D');

            Assert.That(output[4], Is.EqualTo("-C---C-"));
            Assert.That(output[5], Is.EqualTo("--B-B--"));
            Assert.That(output[6], Is.EqualTo("---A---"));
        }

        [Test]
        public void RunZee()
        {
            var diamond = new Diamond();
            var output = diamond.drawDiamond('Z');

            foreach (var line in output)
            {
                Console.WriteLine(line);
            }
        }

    }
}